#pragma once
#ifndef _ORB
#define _ORB
#include "ofMain.h"

class Orb
{
public:
	Orb();
	~Orb();

	void setup();
	void update();
	void draw();
	float getXPos();
	float getYPos();
	void setXPos(float x);
	void setYPos(float y);
	float getSpeedX();
	float getSpeedY();
	void setSpeedY(float speed);
	void setSpeedX(float speed);
	void setSize(float size);
	float getSize();
	ofColor getColor();
	void setColor(ofColor color);

private:
	float x;
	float y;
	float speedX;
	float speedY;
	float xVel;
	float yVel;
	int radius;
	ofColor color;
	float startTime;
	float timer;
	float lifeTime;
};

#endif // !Orb
