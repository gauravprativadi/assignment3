#pragma once

#include "ofMain.h"
#include "Orb.h"
#include "ofxGUI.h"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ofVideoGrabber cam;
		ofImage frame;
		vector<Orb*> orbs;

		ofxIntSlider radius;
		ofxFloatSlider speedX;
		ofxFloatSlider speedY;
		ofxButton clearButton;

		ofxPanel gui;

		int orbs_number = 100;

		float startTime;
		float timer;
};
