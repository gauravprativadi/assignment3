#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	cam.setup(800, 600);
	frame.allocate(800, 600, OF_IMAGE_COLOR);

	gui.setup();
	gui.add(radius.setup("Radius", 20, 5, 50));
	gui.add(speedX.setup("X Velocity", 1, 1, 5));
	gui.add(speedY.setup("Y Velocity", 1, 1, 5));
	gui.add(clearButton.setup("Clear orbs"));
}

//--------------------------------------------------------------
void ofApp::update(){
	if (cam.isInitialized()) {
		cam.update();
		
		if (cam.isFrameNew()) {
			frame.setFromPixels(cam.getPixels());
			
		}

	}

	timer = ofGetElapsedTimeMillis() - startTime;
	if (timer >= 10) {
		startTime = ofGetElapsedTimeMillis();
		if (orbs.size() < 1000) {
			Orb *myOrb = new Orb();
			myOrb->setSpeedX(speedX);
			myOrb->setSpeedY(speedY);
			myOrb->setSize(radius);
			myOrb->setup();
			orbs.push_back(myOrb);
		}
	}


	for (int i = 0; i < orbs.size(); i++) {

		orbs[i]->update();
		if (orbs[i]->getSize() < 0)
		{
			delete orbs[i];
			orbs.erase(orbs.begin() + i);
		}
	}
	if (clearButton) {
		for (int i = 0; i < orbs.size(); i++) {
			delete orbs[i];
			orbs.erase(orbs.begin() + i);
		}
	}


}

//--------------------------------------------------------------
void ofApp::draw(){

	//frame.draw(0, 0);
	for (auto&& orb : orbs) {

		float xPos = orb->getXPos();
		float yPos = orb->getYPos();

		orb->setColor(frame.getColor(xPos, yPos));
		//ofPixel
		orb->draw();
	}
	gui.draw();

}
