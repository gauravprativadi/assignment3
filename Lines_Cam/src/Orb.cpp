#include "Orb.h"

Orb::Orb()
{

}

Orb::~Orb()
{

}

void Orb::setup()
{
	x = ofGetWidth() / 2;
	y = ofRandom(200, ofGetHeight() - 200);
	speedX = xVel * ofRandom(-1, 1);
	speedY = xVel* ofRandom(-1, 1);
	//radius = 20;
	/*color.setHsb(ofRandom(0, 255),
		ofRandom(0, 255),
		ofRandom(0, 255));*/
	startTime = ofGetElapsedTimeMillis();
}

void Orb::update()
{
	if (x > ofGetWidth() - radius - 10 || x < 0 + radius + 10) {
		speedX *= -1;
		//color.setHue(ofRandom(0, 255));
	}
	if (y  > ofGetHeight() - radius - 20 || y < 0 + radius + 10) {
		speedY *= -1;
		//color.setHue(ofRandom(0, 255));

	}
	x += speedX;
	y += speedY;

	timer = ofGetElapsedTimeMillis() - startTime;
	if (timer >= 500) {
		startTime = ofGetElapsedTimeMillis();
		radius--;
	}

}

void Orb::draw()
{
	ofSetColor(color);
	ofDrawCircle(x, y, radius);
}

float Orb::getXPos()
{
	return x;
}

float Orb::getYPos()
{
	return y;
}

void Orb::setXPos(float x)
{
	this->x = x;
}

void Orb::setYPos(float y)
{
	this->y = y;
}

float Orb::getSpeedX()
{
	return speedX;
}

float Orb::getSpeedY()
{
	return speedY;
}

void Orb::setSpeedY(float speed)
{
	yVel = speed;
}

void Orb::setSpeedX(float speed)
{
	xVel = speed;
}

void Orb::setSize(float size)
{
	this->radius = size;
}

float Orb::getSize()
{
	return radius;
}

ofColor Orb::getColor()
{
	return this->color;
}

void Orb::setColor(ofColor color)
{
	this->color = color;
}

